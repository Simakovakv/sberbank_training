package ru.home.simakova.p0011lesson1_activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class InfoActivity extends MainActivity {
    Button buttonComeback;
    TextView infoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        initElements();
        initListeners();
    }
    private void initElements(){
        buttonComeback = findViewById(R.id.buttonComeback);
        infoView = findViewById(R.id.infoView);
    }
    private void initListeners(){
        buttonComeback.setOnClickListener(new ButtonOnClickListener());
    }
    private class ButtonOnClickListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.buttonComeback:
                    infoView.setText("Saved data on Info");
                    Intent intent = new Intent(InfoActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP );
                    startActivity(intent);
                    break;
            }
        }
    }
}
