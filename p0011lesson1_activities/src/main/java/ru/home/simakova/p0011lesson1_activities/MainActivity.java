package ru.home.simakova.p0011lesson1_activities;

import android.app.ActivityManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MyLogs";

    private TextView helloText;
    private Button buttonDetail;
    private ActivityManager am;
    List<ActivityManager.RunningTaskInfo> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "On create");
        setContentView(R.layout.activity_main);
        Log.d(TAG, "Инициализировали Views");
        initElements();
        Log.d(TAG, "Инициализируем обработчик кнопок");
        initListeners();
        am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
    }
    private void initElements(){
        helloText = findViewById(R.id.helloView);
        helloText.setText(R.string.helloView);
        buttonDetail = findViewById(R.id.buttonDetail);
    }
    private void initListeners(){
        buttonDetail.setOnClickListener(new ButtonClickListener());
    }
    private class ButtonClickListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            Log.d(TAG, "По id определяем кнопку, вызвавшую обработчик");
            switch (v.getId()) {
                case R.id.buttonDetail:
                    Log.d(TAG, "Нажата кнопка Detail");
                    list = am.getRunningTasks(10);
                    for (ActivityManager.RunningTaskInfo task : list) {
                        if (task.baseActivity.flattenToShortString().startsWith("ru.home.simakova.p0011lesson1_activities")){
                            Log.d(TAG, "------------------");
                            Log.d(TAG, "Count: " + task.numActivities);
                            Log.d(TAG, "Root: " + task.baseActivity.flattenToShortString());
                            Log.d(TAG, "Top: " + task.topActivity.flattenToShortString());

                        }
                    }
                    helloText.setText("Saved data");
                    startActivity(new Intent(MainActivity.this, DetailActivity.class));
                    break;
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "On stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "On Destroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "On pause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "On resume");
    }
}
