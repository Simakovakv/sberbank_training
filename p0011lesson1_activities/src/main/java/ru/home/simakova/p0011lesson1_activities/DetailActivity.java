package ru.home.simakova.p0011lesson1_activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailActivity extends MainActivity {

    private static final String TAG = "MyLogs";
    private Button buttonInfo;
    private TextView detailView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Загрузили Detail activity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Log.d(TAG, "Инициализировали элементы DetailActivity");
        initElements();
        Log.d(TAG, "Инициализировали обработчики кнопок DetailActivity");
        initListeners();
    }

    private void initElements(){
        buttonInfo = findViewById(R.id.buttonInfo);
        detailView = findViewById(R.id.detailView);
    }
    private void initListeners(){
        buttonInfo.setOnClickListener(new ButtonClickListener());
    }
    private class ButtonClickListener implements View.OnClickListener {
        public void onClick(View v) {
            Log.d(TAG, "По id определяем кнопку, вызвавшую обработчик");
            switch (v.getId()) {
                case R.id.buttonInfo:
                    detailView.setText("Saved data on Detail");
                    Log.d(TAG, "Нажата кнопка Info");
                    startActivity(new Intent(DetailActivity.this, InfoActivity.class));
                    break;
            }
        }
    }
}
